#include <stdio.h>

main() {
	int num1 = 0, num2 = 1, num3 = 0 ;
	int inc, num_elem ;

	printf("Enter the number of elements: ") ;
	scanf("%d", &num_elem) ;
	printf("\n %d %d", num1, num2) ;

	for(inc = 2; inc < num_elem; ++inc) {
		num3 = num1 + num2 ;
		printf(" %d", num3) ;
		num1 = num2 ;
		num2 = num3 ;
	}
	printf("\n") ;
}
