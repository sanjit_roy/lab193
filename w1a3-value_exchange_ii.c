#include <stdio.h>

main()
{
	int arg1, arg2;

	printf("Enter argument 1 : ");
	scanf("%d", &arg1);
	printf("Enter argument 2 : ");
	scanf("%d", &arg2);
	
	printf("Exchanging values \n");

	arg1 = arg1 + arg2;
	arg2 = arg1 - arg2;
	arg1 = arg1 - arg2;
	
	printf("The value of argument 1 is now : %d \n", arg1);
	printf("The value of argument 2 is now : %d \n", arg2);
}
