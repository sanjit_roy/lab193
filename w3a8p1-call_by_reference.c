#include<stdio.h>

void swap( int *x, int *y )                     // call by refrence
{
    int t ;
    t = *x ;
    *x = *y ;
    *y = t ;
    printf( "\nx = %d y = %d", *x,*y);
}

int main( )
{
    int a = 10, b = 20 ;
    swap ( &a, &b ) ;                              // passing the address of values to be swapped
    printf ( "\na = %d b = %d \n", a, b ) ;
}