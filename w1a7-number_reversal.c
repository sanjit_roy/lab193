#include <stdio.h>

main()
{
	int number ;
	int dig1, dig2, dig3;
	int reverseNumber;

	printf("Enter a number : ");
	scanf("%d", &number);

	dig1 = number % 10;
	number = number / 10;
	dig2 = number % 10;
	number = number / 10;
	dig3 = number % 10;
	
	reverseNumber = (dig1*100) + (dig2*10) + dig3;

	printf("The reverse number is : %d \n", reverseNumber) ;
}
