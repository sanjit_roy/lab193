#include <stdio.h>

void main(int argc, char *argv[]){
	long a,b,c ;

	a = strtol(argv[1], NULL, 0) ;
	b = strtol(argv[2], NULL, 0) ;
	c = strtol(argv[3], NULL, 0) ;

	if(a > b && a > c)
		printf("The largest is %d \n", a) ;
	else if(b > c)
		printf("The largest is %d \n", b) ;
	else
		printf("The largest is %d \n", c) ;
}