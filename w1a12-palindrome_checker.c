#include <stdio.h>

main() {
	int arg ;
	int remainder, arg_copy, reversed_number = 0 ;
	
	printf("Enter a number: ") ;
	scanf("%d", &arg) ;

	arg_copy = arg ;

	while(arg != 0){
		remainder = arg % 10 ;
		reversed_number = reversed_number * 10 + remainder ;
		arg = arg / 10 ;
	}

	if(arg_copy == reversed_number)
		printf("%d is a palindrome. \n", arg_copy) ;
	else
		printf("%d is not a palindrome. \n", arg_copy) ;

}
