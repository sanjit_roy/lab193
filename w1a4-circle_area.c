#include <stdio.h>

#define PI 3.141

void main()
{
	float radius;
	float area;

	printf("What is the radius of the circle? ");
	scanf("%f", &radius);

	area = PI * radius * radius;

	printf("The area is: %f \n", area);


}
