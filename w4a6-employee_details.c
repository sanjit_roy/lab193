#include <stdio.h>
#include <stdlib.h>

int main(){
	struct emp{
    	int empno ;
    	char name[10] ;
    	int bpay, dateofjoining ;
	} e[10] ;
    int i, n ;
    printf("Enter the number of employees : ") ;
    scanf("%d", &n) ;
    for(i = 0 ; i < n ; i++)
    {
        printf("\nEnter the employee number : ") ;
        scanf("%d", &e[i].empno) ;
        printf("\nEnter the name : ") ;
        scanf("%s", e[i].name) ;
        printf("\nEnter the basic pay: ") ;
        scanf("%d", &e[i].bpay) ;
        printf("\nEnter the Date of Joining: ") ;
        scanf("%d", &e[i].dateofjoining) ;
    }
    printf("\nEmp. No. Name \t Bpay \t Date of Joining\n\n") ;
    for(i = 0 ; i < n ; i++)
    {
        printf("%d \t %s \t %d \t %d\n", e[i].empno,
        e[i].name, e[i].bpay, e[i].dateofjoining) ;
    }
}