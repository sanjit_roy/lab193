#include<stdio.h>

void writeFile() ;
void readFile() ;

void main()
{
    writeFile() ;   
    readFile() ;
}

void writeFile(){
    FILE *fptr;
    char name[20];
    int age;
    float salary;
 
    /*  open for writing */
    fptr = fopen("emp.txt", "w");
 
    if (fptr == NULL)
    {
        printf("File does not exists \n");
        return;
    }
    printf("Enter the name \n");
    scanf("%s", name);
    fprintf(fptr, "Name  = %s\n", name);
    
    printf("Enter the age\n");
    scanf("%d", &age);
    fprintf(fptr, "Age  = %d\n", age);
    
    printf("Enter the salary\n");
    scanf("%f", &salary);
    fprintf(fptr, "Salary  = %.2f\n", salary);
    
    fclose(fptr);
}


void readFile() {
    FILE *fptr;
    char filename[15];
    char ch;
 
    printf("Trying to read the file recently created.... \n");
    
    /*  open the file for reading */
    fptr = fopen("emp.txt", "r");
    if (fptr == NULL)
    {
        printf("Cannot open file \n");
        exit(0);
    }
    ch = fgetc(fptr);
    while (ch != EOF)
    {
        printf ("%c", ch);
        ch = fgetc(fptr);
    }
    fclose(fptr);
}