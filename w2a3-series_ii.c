#include <stdio.h>
 
void main()
{
    int i, n, nums = 2, sum = 0;
 
    printf("Enter the value of n: ");
    scanf("%d", &n);
    for (i = 1; i <= n; i++)
    {
    	sum = sum + (((i *(i-1))/2)+1) ;
    	//printf("Debug = i:%d, sum:%d\n", i, sum) ;
    }
    printf("\nThe sum is = %d\n", sum);
}