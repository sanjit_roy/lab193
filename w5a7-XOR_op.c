#include <stdio.h>
#include <stdlib.h>

unsigned invert(unsigned x, int p);

int main(void)
{
	int num, pos ;
	printf("Enter a number: ") ;
	scanf("%d", &num) ;
	printf("\nEnter position to flip: ");
	scanf("%d", &pos) ;
	printf("\nConducting XOR...\n") ;
    printf("%u\n", invert(num, pos));
    return EXIT_SUCCESS;
}

/* invert:  returns x with n bits that begin at position p inverted */
unsigned invert(unsigned x, int p)
{
    x = x ^ (1 << p) ;
    return x ;
}