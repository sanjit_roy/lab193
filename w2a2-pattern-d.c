#include <stdio.h>

main() {
	int i, j, rows ;

	printf("Enter number of rows: ") ;
	scanf("%d", &rows);

	for(i=1; i<=rows; ++i){
		for(j=i; j>=1; --j){
			printf("%d ",i) ;
		}
		printf("\n") ;
	}
}
