#include <stdio.h>

void main(){
	int i, j, rows;
	printf("Enter number of rows: ");
	scanf("%d", &rows);
	for(i=rows; i>0; i--){
		for(j=rows; j>=i; j--){
			printf("%d", j);
		}
		for(j=i+1;j<=rows;j++){
			printf("%d", j);
		}
		printf("\n");
	}
	
}
