#include <stdio.h>

main() {
	int arg ;
	int inc = 1, sum = 0;

	printf("Enter a number: ") ;
	scanf("%d", &arg) ;

	while(inc < arg){
		if(arg % inc == 0){
			sum = sum + inc ;
		}
		inc++ ;
	}

	if(sum == arg)
		printf("%d is a perfect number. \n", arg) ;
	else
		printf("%d is not a perfect number. \n", arg) ;

}
