#include <stdio.h>

main()
{
	int princ, rate, time;
	int interest;
	
	printf("Enter principal : ");
	scanf("%d", &princ);
	printf("Enter rate : ");
	scanf("%d", &rate);
	printf("Enter time : ");
	scanf("%d", &time);

	interest = (princ * rate * time) / 100;
	
	printf("Simple Interest : %d", interest);
	printf("\n");
}
