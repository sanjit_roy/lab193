#include <stdio.h>

main(){
	int arg1, arg2 ;
	int menu_choice ;
	int result ;

	printf("Enter the number for the operation: \n") ;
	printf("1. Addition \n") ;
	printf("2. Substraction \n") ;
	printf("3. Multiplication \n") ;
	printf("4. Division \n") ;

	scanf("%d", &menu_choice) ;

	printf("Enter the first number: \n") ;
	scanf("%d", &arg1) ;
	printf("Enter the second number: \n") ;
	scanf("%d", &arg2) ;

	switch(menu_choice){
		case 1:
			result = arg1 + arg2 ;
			break ;

		case 2:
			result = arg1 - arg2 ;
			break ;

		case 3:
			result = arg1 * arg2 ;
			break ;

		case 4:
			result = arg1 / arg2 ;
			break ;

		default:
			result = 0 ;
			break ;
	}

	printf("The result is: %d \n", result) ;
}
