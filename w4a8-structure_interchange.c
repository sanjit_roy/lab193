#include <stdio.h>
#include <stdlib.h>

int main(){
	typedef struct {
            int x[5];
            } intarr;
            intarr a, b, temp;
    int i;
    for (i = 0; i < 5; i++) {
        a.x[i] = i+1;
        b.x[i] = 11*(i+1);
    }
    printf("Initially:\n");
    printf("a = ");
    for (i = 0; i < 5; i++) {
        printf("  %3d", a.x[i]);
    }
    printf("\n");
    printf("b = ");
    for (i = 0; i < 5; i++) {
        printf("  %3d", b.x[i]);
    }
    printf("\n\n");

    /* Now exchange the contents of the structs */
    temp = a;
    a    = b;
    b    = temp;

    printf("After exchange:\n");
    printf("a = ");
    for (i = 0; i < 5; i++) {
        printf("  %3d", a.x[i]);
    }
    printf("\n");
    printf("b = ");
    for (i = 0; i < 5; i++) {
        printf("  %3d", b.x[i]);
    }
    printf("\n");
}