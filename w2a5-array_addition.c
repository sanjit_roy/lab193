#include <stdio.h>

void main() {
	int i, arr1[5] = {1, 3, 5, 7, 9}, arr2[5] = {2, 4, 6, 8, 10}, result[5];
	printf("Array1\tArray2\tResult\n");
	for(i=0;i<5;i++){
		result[i]= arr1[i] + arr2[i];
		printf("%d\t%d\t%d\n", arr1[i], arr2[i], result[i]);
	}
	
}
