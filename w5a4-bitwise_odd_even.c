#include <stdio.h>
#include <string.h>
 
int isOdd (int n)
{
  if (n & 1)
    return 1;
  else
    return 0;
}
 
int main(void)
{
  unsigned int n;
  printf("Enter a positive integer: ");
  scanf("%u", &n);
  if (isOdd(n))
    printf("%d is odd\n", n);
  else
    printf("%d is even\n", n);
}