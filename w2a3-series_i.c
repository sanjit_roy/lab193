#include <stdio.h>
 
void main()
{
    int i, n, nums = 2, even_sum = 0;
 
    printf("Enter the value of n\n");
    scanf("%d", &n);
    for (i = 1; i <= n; i++)
    {
        even_sum = even_sum + nums;
        nums = nums + 2 ;
    }
    printf("Sum of all even numbers = %d\n", even_sum);
}