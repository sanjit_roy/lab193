#include <stdio.h>

void main(){
	int number=100, tmp;
	int digit1, digit2, digit3;
	
	printf("The 3 digit Armstrong numbers are: ");
	while(number <=999)
	{
		digit1 = number - ((number / 10) * 10);
		digit2 = (number / 10) - ((number / 100) * 10);
		digit3 = (number / 100) - ((number / 1000) * 10);
		tmp = (digit1 * digit1 * digit1) + (digit2 * digit2 * digit2) + (digit3 * digit3 * digit3);
	       	if(tmp == number){
			printf("\t %d", tmp);
		}
		number++;
	}	
	printf("\n");
}
