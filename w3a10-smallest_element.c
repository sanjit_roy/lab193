#include <stdio.h> 

int getSmallest(int arr[], int size) ;

int main() 
{
    int array[100], minimum, s, c, location = 1;
    printf("Enter the number of elements in array\n");
    scanf("%d",&s);
 
    printf("Enter %d integers\n", s);
 
    for ( c = 0 ; c < s ; c++ )
        scanf("%d", &array[c]);
 
    minimum = getSmallest(array, s) ;
    printf("Minimum element's value is %d.\n", minimum);
    return 0;
}


int getSmallest(int array[], int size) {
    int minimum, c, location = 1;
    minimum = array[0];
 
    for ( c = 1 ; c < size ; c++ ) 
    {
        if ( array[c] < minimum ) 
        {
           minimum = array[c];
           location = c+1;
        }
    } 

    return minimum ;
}