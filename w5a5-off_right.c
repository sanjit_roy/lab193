#include <stdio.h>
#include <stdlib.h>

unsigned invert(unsigned x, int p);

int main(void)
{
	int num, pos ;
	printf("Enter a number: ") ;
	scanf("%d", &num) ;
	printf("\nConducting right shift clear...\n") ;
    printf("%u\n", invert(num, 11));
    return EXIT_SUCCESS;
}

/* invert:  returns x with n bits that begin at position p inverted */
unsigned invert(unsigned x, int p)
{
    x = x & p ;
    return x ;
}